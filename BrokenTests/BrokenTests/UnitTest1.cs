﻿using System;
using System.Threading;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.Extensions;


namespace BrokenTests
{
    [TestFixture]
    public class UnitTest1
    {
        private IWebDriver driver;
        private Logger log;

        [OneTimeSetUp]
        public void BeforeAnyTests()
        {
            log = new Logger();
            driver = new ChromeDriver();
            log.Write("I finished running the OneTimeSetUp!");
        }

        [OneTimeTearDown]
        public void AfterAllTests()
        {
            driver.Dispose();
            log.Write("I finished disposing of the driver and running OneTimeTearDown!");
        }

        [SetUp]
        public void BeforeEachTest()
        {
            driver.Navigate().GoToUrl("http://www.google.com");
            log.Write("I successfully navigated to Google in SetUp to ensure a clean state!");
        }

        [TearDown]
        public void AfterEachTest()
        {
            string workDir = TestContext.CurrentContext.WorkDirectory;
            string testName = TestContext.CurrentContext.Test.Name;

            if (!TestContext.CurrentContext.Result.Outcome.Equals(ResultState.Success))
            {
                log.Write($"The test {testName} failed :(. Stacktrace follows.");
                log.Write(TestContext.CurrentContext.Result.StackTrace);
                driver
                .TakeScreenshot()
                .SaveAsFile($"{workDir}\\FailureScreenshot{testName}.png");
            }
            log.Write("I finished running the TearDown method!");
        }

        /// <summary>
        /// This test navigates to the dynamic loading page, clicks the start button, and verifies Hello World! is displayed.
        /// </summary>
        [Test]
        public void ShouldVerifyText_WhenButtonClicked()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/dynamic_loading/1");
            driver.FindElement(By.CssSelector("#start > button")).Click();
            Thread.Sleep(10000);
            var finishText = driver.FindElement(By.CssSelector("#finish > h4"));
            Assert.AreEqual("Hello World!", finishText.Text, "The text displayed after the loading bar went away was not correct!");
        }

        /// <summary>
        /// This test navigates to demoqa.com, clicks on the Registration button on the right, and verifies it navigated to the correct page
        /// </summary>
        [Test]
        public void ShouldNavigateToRegistrationPage_WhenRegistrationButtonClicked()
        {
            driver.Navigate().GoToUrl("http://demoqa.com/");
            driver.FindElement(By.Id("menu-item-374")).Click();
            
            Assert.AreEqual("http://demoqa.com/registration/", driver.Url, "We did not navigate to the registration page after clicking the button!");
        }

        /// <summary>
        /// This test navigates to the registration page of demoqa.com and verifies the first name textbox is visible.
        /// </summary>
        [Test]
        public void ShouldDisplayNameField_WhenRegistrationPageDisplayed()
        {
            driver.Navigate().GoToUrl("http://demoqa.com/registration/");
            var firstNameTextBox = driver.FindElement(By.Id("name_3_firstname"));
            Assert.AreEqual("http://demoqa.com/registration/", driver.Url, "We Navigated to textbox!");


        }

    }
}
